// dtkCoreParameter.h
//

#pragma once

#include <dtkCoreExport>

#include "dtkCoreParameters.h"
#include "dtkCoreTypeTraits.h"

#include <QtCore>

#include <array>

class dtkCoreParameter;

// ///////////////////////////////////////////////////////////////////
// MACRO TO REGISTER PARAMETER TO QMETATYPE SYSTEM
// ///////////////////////////////////////////////////////////////////

// MACRO IN HEADER FILE
#define DTK_DECLARE_PARAMETER(type) \
    Q_DECLARE_METATYPE(type)        \
    Q_DECLARE_METATYPE(type*)

// MACRO IN CPP FILE
#define DTK_DEFINE_PARAMETER(type, name_space)                          \
    namespace dtk {                                                     \
        namespace detail {                                              \
            namespace name_space {                                      \
                int m_dummy = dtkCoreParameter::registerToMetaType<type>(); \
            }                                                           \
        }                                                               \
    }

// ///////////////////////////////////////////////////////////////////
// Typetraits
// ///////////////////////////////////////////////////////////////////

namespace dtk {
    template <typename U, typename V = void>
    using parameter_arithmetic = std::enable_if_t<std::is_arithmetic<U>::value, V>;
    template <typename U, typename V = void>
    using parameter_not_arithmetic = std::enable_if_t<!std::is_arithmetic<U>::value, V>;
    template <typename U, typename V = void>
    using is_core_parameter = std::enable_if_t<std::is_base_of<dtkCoreParameter, std::remove_pointer_t<std::decay_t<U>>>::value, V>;
}

namespace dtk {
    namespace core {
        DTKCORE_EXPORT dtkCoreParameters readParameters(const QString&);
    }
}

// ///////////////////////////////////////////////////////////////////
// Helper class managing connection
// ///////////////////////////////////////////////////////////////////

class DTKCORE_EXPORT dtkCoreParameterConnection : public QObject
{
    Q_OBJECT

signals:
    void valueChanged(QVariant);
    void invalidValue(void);

public:
    QVector<QMetaObject::Connection> value_list;
    QVector<QMetaObject::Connection> invalid_list;
    QVector<dtkCoreParameter *>      param_list;
};

Q_DECLARE_METATYPE(std::shared_ptr<dtkCoreParameterConnection>);

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameter interface
// ///////////////////////////////////////////////////////////////////

class DTKCORE_EXPORT dtkCoreParameter
{
public:
    using connection = std::shared_ptr<dtkCoreParameterConnection>;

public:
     dtkCoreParameter(void) = default;
     dtkCoreParameter(const QString&, const QString& = QString());
     dtkCoreParameter(const dtkCoreParameter&);
    ~dtkCoreParameter(void) = default;

    virtual QString typeName(void) const = 0;

    void setUid(const QString&);
    const QString& uid(void) const;

    void setLabel(const QString&);
    QString label(void) const;

    void setDocumentation(const QString&);
    QString documentation(void) const;

    virtual void setValue(const QVariant&) = 0;
    virtual QVariant variant(void) const = 0;
    virtual QVariantHash toVariantHash(void) const = 0;

#pragma mark - Connection management

    void block(bool);
    void sync(void);
    template <typename F> QMetaObject::Connection connect(F);
    void disconnect(void);

    void syncFail(void);
    template <typename F> QMetaObject::Connection connectFail(F);
    void disconnectFail(void);

    bool shareConnectionWith(dtkCoreParameter *);
    void shareValue(QVariant);

    virtual void copyAndShare(dtkCoreParameter *) = 0;
    virtual void copyAndShare(const QVariant&) = 0;

#pragma mark - Factory method

public:
    template <typename T>
    static int registerToMetaType(void);
    static dtkCoreParameter *create(const QVariantHash&);

protected:
    QString m_uid;
    QString m_label;
    QString m_doc;
    connection m_connection;

    mutable bool m_enable_share_connection = true;
};

Q_DECLARE_METATYPE(dtkCoreParameter *);

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameterBase CRTP class
// ///////////////////////////////////////////////////////////////////

template <typename Derive>
class dtkCoreParameterBase : public dtkCoreParameter
{
public:
     dtkCoreParameterBase(void) = default;
     dtkCoreParameterBase(const QString&, const QString& = QString());
     dtkCoreParameterBase(const dtkCoreParameterBase&);
    ~dtkCoreParameterBase(void) = default;

public:
    QString typeName(void) const override;

    QVariant variant(void) const final;
    void copyAndShare(dtkCoreParameter *) final;
    void copyAndShare(const QVariant&) final;

    virtual QVariantHash toVariantHash(void) const override = 0;
};

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameter simple version
// ///////////////////////////////////////////////////////////////////

template <typename T, typename Enable = void>
class dtkCoreParameterSimple : public dtkCoreParameterBase<dtkCoreParameterSimple<T>>
{
public:
    using value_type = T;
    using self_type = dtkCoreParameterSimple<value_type>;
    using base_type = dtkCoreParameterBase<self_type>;

    using dtkCoreParameter::documentation;
    using dtkCoreParameter::setDocumentation;
    using dtkCoreParameter::label;
    using dtkCoreParameter::setLabel;

public:
     dtkCoreParameterSimple(void) = default;
    ~dtkCoreParameterSimple(void) = default;

    dtkCoreParameterSimple(const dtkCoreParameter *);
    dtkCoreParameterSimple(const QVariant&);
    dtkCoreParameterSimple(const dtkCoreParameterSimple&);

    dtkCoreParameterSimple(const QString&, const T&, const QString& = QString());

    dtkCoreParameterSimple& operator = (const T&);
    dtkCoreParameterSimple& operator = (const dtkCoreParameter *);
    dtkCoreParameterSimple& operator = (const QVariant&);
    dtkCoreParameterSimple& operator = (const dtkCoreParameterSimple&);

    operator T() const;

    void setValue(const T&);
    void setValue(const QVariant&) override;

    T value(void) const;

    QVariantHash toVariantHash(void) const override;

private:
    using dtkCoreParameter::m_label;
    using dtkCoreParameter::m_doc;

    T m_value = T();
};

template <typename T>
DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterSimple<T>&);
template <typename T>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterSimple<T>&);

template <typename T>
DTKCORE_EXPORT QDebug operator << (QDebug, dtkCoreParameterSimple<T>);

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameterNumeric for arithmetic types
// ///////////////////////////////////////////////////////////////////

template <typename T, typename E = dtk::parameter_arithmetic<T>>
class dtkCoreParameterNumeric : public dtkCoreParameterBase<dtkCoreParameterNumeric<T>>
{
public:
    using value_type = T;
    using self_type = dtkCoreParameterNumeric<value_type>;
    using base_type = dtkCoreParameterBase<self_type>;

    using dtkCoreParameter::documentation;
    using dtkCoreParameter::setDocumentation;
    using dtkCoreParameter::label;
    using dtkCoreParameter::setLabel;

public:
     dtkCoreParameterNumeric(void) = default;
    ~dtkCoreParameterNumeric(void) = default;

    dtkCoreParameterNumeric(const T&);
    template <typename U, typename V = typename dtk::is_core_parameter<U>>
    dtkCoreParameterNumeric(const U *);
    dtkCoreParameterNumeric(const QVariant&);
    dtkCoreParameterNumeric(const dtkCoreParameterNumeric&);

    dtkCoreParameterNumeric(const QString&, const T&, const T&, const T&, const QString& doc = QString());
#ifndef SWIG
    template <typename U = T, typename = std::enable_if_t<std::is_floating_point<U>::value>> dtkCoreParameterNumeric(const QString&, const T&, const T&, const T&, const int&, const QString& doc = QString());
#endif
    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator = (const U&);
    template <typename U> dtk::is_core_parameter<U, dtkCoreParameterNumeric&> operator = (const U *);
    dtkCoreParameterNumeric& operator = (const QVariant&);
    dtkCoreParameterNumeric& operator = (const dtkCoreParameterNumeric&);
    template <typename U> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator = (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator += (const U&);
    dtkCoreParameterNumeric& operator += (const QVariant&);
    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator += (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator -= (const U&);
    dtkCoreParameterNumeric& operator -= (const QVariant&);
    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator -= (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator *= (const U&);
    dtkCoreParameterNumeric& operator *= (const QVariant&);
    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator *= (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator /= (const U&);
    dtkCoreParameterNumeric& operator /= (const QVariant&);
    template <typename U> dtk::parameter_arithmetic<U, dtkCoreParameterNumeric&> operator /= (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> std::enable_if_t<std::numeric_limits<U>::is_modulo && std::numeric_limits<T>::is_modulo, dtkCoreParameterNumeric&> operator %= (const U&);
    template <typename U = T> std::enable_if_t<std::numeric_limits<U>::is_modulo, dtkCoreParameterNumeric&> operator %= (const QVariant&);
    template <typename U = T> std::enable_if_t<std::numeric_limits<U>::is_modulo && std::numeric_limits<T>::is_modulo, dtkCoreParameterNumeric&> operator %= (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, bool> operator == (const U&);
    bool operator == (const QVariant&);
    template <typename U> dtk::parameter_arithmetic<U, bool> operator == (const dtkCoreParameterNumeric<U>&);

    template <typename U = T> dtk::parameter_arithmetic<U, bool> operator != (const U&);
    bool operator != (const QVariant&);
    template <typename U> dtk::parameter_arithmetic<U, bool> operator != (const dtkCoreParameterNumeric<U>&);

    operator T() const;

    void setValue(const T&);
    void setValue(const QVariant&) override;

    T value(void) const;

    T min(void) const;
    T max(void) const;

    void setMin(const T&);
    void setMax(const T&);

    const std::array<T, 2>& bounds(void) const;

    void setBounds(const std::array<T, 2>&);

    template <typename U = T> std::enable_if_t<std::is_floating_point<U>::value> setDecimals(const int&);

    int decimals(void) const;

    QVariantHash toVariantHash(void) const override;

protected:
    using dtkCoreParameter::m_label;
    using dtkCoreParameter::m_doc;

    T m_val = T(0);
    std::array<T, 2> m_bounds = {std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max()};
    int m_decimals = std::numeric_limits<T>::max_digits10/1.75; // 9 decimals for double, 5 for float
};

template <typename T>
DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterNumeric<T>&);
template <typename T, typename E = std::enable_if_t<std::is_floating_point<T>::value>>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterNumeric<T>&);
template <typename T, typename E = std::enable_if_t<!std::is_floating_point<T>::value>, typename U = T>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterNumeric<T>&);

DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterNumeric<char>&);
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterNumeric<char>&);

template <typename T>
DTKCORE_EXPORT QDebug operator << (QDebug, dtkCoreParameterNumeric<T>);

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameter contained in a given list
// ///////////////////////////////////////////////////////////////////

template <typename T>
class dtkCoreParameterInList : public dtkCoreParameterBase<dtkCoreParameterInList<T>>
{
public:
    using value_type = T;
    using self_type = dtkCoreParameterInList<value_type>;
    using base_type = dtkCoreParameterBase<self_type>;

    using dtkCoreParameter::documentation;
    using dtkCoreParameter::setDocumentation;
    using dtkCoreParameter::label;
    using dtkCoreParameter::setLabel;

public:
     dtkCoreParameterInList(void) = default;
    ~dtkCoreParameterInList(void) = default;

    dtkCoreParameterInList(const T&);
    dtkCoreParameterInList(const QVariant&);
    dtkCoreParameterInList(const dtkCoreParameterInList&);

    dtkCoreParameterInList(const QString&, const T&, const QList<T>&, const QString& = QString());
    dtkCoreParameterInList(const QString&, int, const QList<T>&, const QString& = QString());
    dtkCoreParameterInList(const QString&, const QList<T>&, const QString& = QString());

    dtkCoreParameterInList& operator = (const T&);
    dtkCoreParameterInList& operator = (const QVariant&);
    dtkCoreParameterInList& operator = (const dtkCoreParameterInList&);

    operator T() const;

    void setValueIndex(int);
    void setValue(const T&);
    void setValue(const QVariant&) override;
    void setValues(const QList<T>&);

    int valueIndex(void) const;
    T value(void) const;
    QList<T> values(void) const;

    QVariantHash toVariantHash(void) const override;


private:
    using dtkCoreParameter::m_label;
    using dtkCoreParameter::m_doc;

    QList<T> m_values;
    int m_value_index = -1;
};

template <typename T>
DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterInList<T>&);
template <typename T>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterInList<T>&);

template <typename T>
DTKCORE_EXPORT QDebug operator << (QDebug, dtkCoreParameterInList<T>);

// ///////////////////////////////////////////////////////////////////
// dtkCoreParameterRange declaration
// ///////////////////////////////////////////////////////////////////

template <typename T, typename E = dtk::parameter_arithmetic<T>>
class dtkCoreParameterRange : public dtkCoreParameterBase<dtkCoreParameterRange<T>>
{
public:
    using range = std::array<T, 2>;
    using value_type = T;
    using self_type = dtkCoreParameterRange<value_type>;
    using base_type = dtkCoreParameterBase<self_type>;

    using dtkCoreParameter::documentation;
    using dtkCoreParameter::setDocumentation;
    using dtkCoreParameter::label;
    using dtkCoreParameter::setLabel;

public:
     dtkCoreParameterRange(void) = default;
    ~dtkCoreParameterRange(void) = default;

    dtkCoreParameterRange(const std::array<T, 2>&);
    dtkCoreParameterRange(std::initializer_list<T>);
    dtkCoreParameterRange(const QVariant&);
    dtkCoreParameterRange(const dtkCoreParameterRange&);

    dtkCoreParameterRange(const QString&, const std::array<T, 2>&, const T&, const T&, const QString& doc = QString());
#ifndef SWIG
    template <typename U = T, typename = std::enable_if_t<std::is_floating_point<U>::value>> dtkCoreParameterRange(const QString&, const std::array<T, 2>&, const T&, const T&, const int&, const QString& doc = QString());
#endif
    dtkCoreParameterRange& operator = (const std::array<T, 2>&);
    dtkCoreParameterRange& operator = (std::initializer_list<T>);
    dtkCoreParameterRange& operator = (const QVariant&);
    dtkCoreParameterRange& operator = (const dtkCoreParameterRange&);

    void setValue(const std::array<T, 2>&);
    void setValue(std::initializer_list<T>);
    void setValue(const QVariant&) override;

    const std::array<T, 2>& value(void) const;

    T min(void) const;
    T max(void) const;

    void setMin(const T&);
    void setMax(const T&);

    const std::array<T, 2>& bounds(void) const;

    void setBounds(const std::array<T, 2>&);

    template <typename U = T> std::enable_if_t<std::is_floating_point<U>::value> setDecimals(const int&);

    int decimals(void) const;

    QVariantHash toVariantHash(void) const override;

private:
    using dtkCoreParameter::m_label;
    using dtkCoreParameter::m_doc;

    std::array<T, 2> m_val = {T(0), T(0)};
    std::array<T, 2> m_bounds = {std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max()};
    int m_decimals = std::numeric_limits<T>::max_digits10/1.75; // 9 decimals for double, 5 for float
};

template <typename T>
DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterRange<T>&);
template <typename T, typename E = std::enable_if_t<std::is_floating_point<T>::value>>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterRange<T>&);
template <typename T, typename E = std::enable_if_t<!std::is_floating_point<T>::value>, typename U = T>
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterRange<T>&);

DTKCORE_EXPORT QDataStream& operator << (QDataStream&, const dtkCoreParameterRange<char>&);
DTKCORE_EXPORT QDataStream& operator >> (QDataStream&, dtkCoreParameterRange<char>&);

template <typename T>
DTKCORE_EXPORT QDebug operator << (QDebug, dtkCoreParameterRange<T>);

// ///////////////////////////////////////////////////////////////////
// Typedef
// ///////////////////////////////////////////////////////////////////

namespace dtk {
    using d_uchar = dtkCoreParameterNumeric<unsigned char>;
    using d_char = dtkCoreParameterNumeric<char>;
    using d_uint = dtkCoreParameterNumeric<qulonglong>;
    using d_int = dtkCoreParameterNumeric<qlonglong>;
    using d_real = dtkCoreParameterNumeric<double>;
    using d_bool = dtkCoreParameterNumeric<bool>;

    using d_string = dtkCoreParameterSimple<QString>;

    using d_inliststring = dtkCoreParameterInList<QString>;
    using d_inlistreal = dtkCoreParameterInList<double>;
    using d_inlistint = dtkCoreParameterInList<qlonglong>;

    using d_range_uchar = dtkCoreParameterRange<unsigned char>;
    using d_range_char = dtkCoreParameterRange<char>;
    using d_range_uint = dtkCoreParameterRange<qulonglong>;
    using d_range_int = dtkCoreParameterRange<qlonglong>;
    using d_range_real = dtkCoreParameterRange<double>;
}

// ///////////////////////////////////////////////////////////////////
// Registration to QMetaType system
// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_PARAMETER(dtk::d_uchar);
DTK_DECLARE_PARAMETER(dtk::d_char);
DTK_DECLARE_PARAMETER(dtk::d_uint);
DTK_DECLARE_PARAMETER(dtk::d_int);
DTK_DECLARE_PARAMETER(dtk::d_real);
DTK_DECLARE_PARAMETER(dtk::d_bool);
DTK_DECLARE_PARAMETER(dtk::d_string);

DTK_DECLARE_PARAMETER(dtk::d_inliststring);
DTK_DECLARE_PARAMETER(dtk::d_inlistreal);
DTK_DECLARE_PARAMETER(dtk::d_inlistint);

DTK_DECLARE_PARAMETER(dtk::d_range_uchar);
DTK_DECLARE_PARAMETER(dtk::d_range_char);
Q_DECLARE_METATYPE(dtk::d_range_char::range);
DTK_DECLARE_PARAMETER(dtk::d_range_uint);
DTK_DECLARE_PARAMETER(dtk::d_range_int);
Q_DECLARE_METATYPE(dtk::d_range_int::range);
DTK_DECLARE_PARAMETER(dtk::d_range_real);

// ///////////////////////////////////////////////////////////////////

#include "dtkCoreParameter.tpp"

//
// dtkCoreParameter.h ends here
